package be.kdg.findfirst;

import be.kdg.data.Artikel;
import be.kdg.data.Artikels;

import java.util.List;
import java.util.Optional;

public class FindFirstFluent {
	public static void main(String[] args) {
		List<Artikel> artikels = Artikels.getArtikels();


// directly print in stream
		artikels
			.stream()
			.filter(e -> e.getPrijs() < 150)
			.filter(e -> e.getMerk().equals("Lenovo"))
			.findFirst()
			.ifPresent(System.out::println)
		;
// also print if nothing found
//        .ifPresentOrElse(
//        	a -> System.out.println(a),() -> System.out.println("Geen artikel gevonden!"));

// What if i want to return a string instead of printin in the chain?
//	    String artikel =  artikels
//		    .stream()
//		    .filter(e -> e.getPrijs() < 150)
//		    .filter(e -> e.getMerk().equals("Lenovo"))
//		    .findFirst()
//		    .map(e -> e.toString())
//		    .orElse("Geen artikel gevonden!");
//	    System.out.println(artikel);

	}
}

/*
  3 - Lenovo  - IdeaPad Z70-80          - € 499,00
*/